<form method="POST">
    <input type="hidden" name="id" value="<?php echo $tarefa['id']; ?>">
    <fieldset>
        <legend>Nova Tarefa</legend>
        <label>
            Tarefa:
            <?php if($tem_erros && isset($erros_validacao['nome'])) : ?>
                <span class="erro">
                    <?php echo $erros_validacao['nome']; ?>
                </span>
            <?php endif; ?>
            <input type="text" name="nome" value="<?php echo $tarefa['nome']; ?>"><br>
        </label>
        <label>
            Descrição (Opcional): <br><br>
            <textarea name="descricao">
                <?php echo $tarefa['descricao']; ?>
            </textarea><br>
        </label>
        <label>
            Prazo (Opcional):
            <?php if($tem_erros && isset($erros_validacao['prazo'])) : ?>
                <span class="erro">
                    <?php echo $erros_validacao['prazo']; ?>
                </span>
            <?php endif; ?>
            <input type="text" name="prazo" value="<?php echo date_database_to_table($tarefa['prazo']); ?>">
        </label>

        <fieldset>
            <legend>Prioridade:</legend>
            <label>
                <input type="radio" name="prioridade" value="1" <?php echo ($tarefa['prioridade'] == 1)
                    ? 'checked'
                    : '';
                ?> >
                Baixa

                <input type="radio" name="prioridade" value="2" <?php echo ($tarefa['prioridade'] == 2)
                    ? 'checked'
                    : '';
                ?> >
                Média

                <input type="radio" name="prioridade" value="3" <?php echo ($tarefa['prioridade'] == 3)
                    ? 'checked'
                    : '';
                ?> >
                Alta
            </label>
        </fieldset>

        <label>
            Tarefa Concluída:
            <input type="checkbox" name="concluida" value="1" <?php echo ($tarefa['concluida'] == 1)
                    ? 'checked'
                    : '';
            ?> >
        </label><br>

        <label>
            Lembrete por e-mail:
            <input type="checkbox" name="lembrete" value="1">
        </label>

        <input type="submit" value="<?php echo ($tarefa['id'] > 0) ? 'Atualizar' : 'Cadastrar'; ?>"> <br>
    </fieldset>
</form>