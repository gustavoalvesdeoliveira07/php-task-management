<?php

$conect = mysqli_connect(BD_SERVER, BD_USER, BD_PASSWORD, BD_DATABASE);

if (mysqli_connect_errno($conect)) {
    echo "Problemas para conectar no banco. Verifique os dados!";
    die();
}

function get_tarefas($conect) {
    $sqlQuery = 'SELECT * FROM tarefa';
    $result = mysqli_query($conect, $sqlQuery);

    $tarefas = array();

    if (!$result) return $tarefas;

    while($tarefa = mysqli_fetch_assoc($result)) {
        $tarefas[] = $tarefa;
    }

    return $tarefas;
}

function insert_tarefa($conect, $tarefa) {
    $sqlQuery = "INSERT INTO tarefa (nome, descricao, prazo, prioridade, concluida) 
    VALUES (
        '{$tarefa['nome']}',
        '{$tarefa['descricao']}',
        '{$tarefa['prazo']}',
        '{$tarefa['prioridade']}',
        '{$tarefa['concluida']}'
    )";

    mysqli_query($conect, $sqlQuery);
}

function get_tarefa($conect, $id) {
    $sqlQuery = 'SELECT * FROM tarefa WHERE id = ' . $id;
    $result = mysqli_query($conect, $sqlQuery);

    return mysqli_fetch_assoc($result);
}

function edit_tarefa($conect, $tarefa) {
    $sqlQuery = "UPDATE tarefa SET
                    nome = '{$tarefa['nome']}',
                    descricao = '{$tarefa['descricao']}',
                    prazo = '{$tarefa['prazo']}',
                    prioridade = '{$tarefa['prioridade']}',
                    concluida = '{$tarefa['concluida']}'
                WHERE id = {$tarefa['id']}";

    mysqli_query($conect, $sqlQuery);
}

function remove_tarefa($conect, $id) {
    $sqlQuery = 'DELETE FROM tarefa WHERE id = ' . $id;

    mysqli_query($conect, $sqlQuery);
}

// ---------- Anexos ----------
function insert_anexo($conect, $anexo) {
    $sqlQuery = "INSERT INTO anexo(tarefa_id, nome, arquivo) VALUES (
        {$anexo['tarefa_id']},
        '{$anexo['nome']}',
        '{$anexo['arquivo']}'
    )";

    mysqli_query($conect, $sqlQuery);
}

function get_anexos($conect, $tarefa_id) {
    $sqlQuery = "SELECT * FROM anexo WHERE tarefa_id = {$tarefa_id}";
    $result = mysqli_query($conect, $sqlQuery);

    $anexos = array();

    if (!$result) return $anexos;

    while ($anexo = mysqli_fetch_assoc($result)) {
        $anexos[] = $anexo;
    }

    return $anexos;
}

function remove_anexo($conect, $id) {
    $sqlQuery = 'DELETE FROM anexo WHERE id = ' . $id;

    mysqli_query($conect, $sqlQuery);
}

?>