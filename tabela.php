<table>
    <tr>
        <th>Tarefas</th>
        <th>Descrição</th>
        <th>Prazo</th>
        <th>Prioridade</th>
        <th>Concluída</th>
        <th>Opções</th>
    </tr>
    <?php foreach($lista_tarefas as $tarefa) : ?>
    <tr>
        <td><?php echo $tarefa['nome']; ?></td>

        <td><?php echo $tarefa['descricao']; ?></td>

        <td><?php echo date_database_to_table($tarefa['prazo']); ?></td>

        <td><?php echo get_prioridade($tarefa['prioridade']); ?></td>

        <td><?php echo get_situacao($tarefa['concluida']); ?></td>
        
        <td>
            <a href="editar.php?id=<?php echo $tarefa['id']; ?>">
                Editar
            </a>
        </td>
        <td>
            <a href="remover.php?id=<?php echo $tarefa['id']; ?>">
                Remover
            </a>
        </td>
        <td>
            <a href="tarefa.php?id=<?php echo $tarefa['id']; ?>">
                <?php echo $tarefa['nome']; ?>
            </a>
        </td>
    </tr>
    <?php endforeach; ?>
</table>