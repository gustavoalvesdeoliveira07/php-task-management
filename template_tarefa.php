<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gerenciador de Tarefas</title>
    <link rel="stylesheet" href="tarefa.css">
</head>
<body>
    <h1>Tarefa: <?php echo $tarefa['nome']; ?></h1>
    <p>
        <a href="tarefas.php">Voltar para a lista de tarefas</a>
    </p>

    <p>
        <strong>Concluída:</strong>
        <?php echo get_situacao($tarefa['concluida']); ?>
    </p>

    <p>
        <strong>Descrição:</strong>
        <?php echo nl2br($tarefa['descricao']); ?>
    </p>

    <p>
        <strong>Prazo:</strong>
        <?php echo date_database_to_table($tarefa['prazo']); ?>
    </p>

    <p>
        <strong>Prioridade:</strong>
        <?php echo get_prioridade($tarefa['prioridade']); ?>
    </p>


    <h2>Anexos</h2>
    <!-- Lista de anexos -->
    <?php if (count($anexos) > 0) : ?>
        <table>
            <tr>
                <th>Arquivo</th>
                <th>Opções</th>
            </tr>

            <?php foreach ($anexos as $anexo) : ?>
                <tr>
                    <td><?php echo $anexo['nome']; ?></td>
                    <td>
                        <a href="anexos/<?php echo $anexo['arquivo']; ?>">
                            Download
                        </a>
                    </td>
                    <td>
                        <a href="remover.php?anexo_id=<?php echo $anexo['id']; ?>">
                            Remover
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>

        </table>
    <?php else : ?>
        <p>Não há anexos para esta tarefa.</p>
    <?php endif; ?>


    <!-- Formulário para um novo anexo -->
    <form action="" method="POST" enctype="multipart/form-data">
        <fieldset>
            <legend>Novo Anexos</legend>

            <input type="hidden" name="tarefa_id" value="<?php echo $tarefa['id']; ?>">

            <label>
                <?php if ($tem_erros && isset($erros_validacao['anexo'])) : ?>
                    <span class="erro">
                        <?php echo $erros_validacao['anexo']; ?>
                    </span>
                <?php endif; ?>

                <input type="file" name="anexo">
            </label>

            <input type="submit" value="Cadastrar">
        </fieldset>
    </form>
</body>
</html>