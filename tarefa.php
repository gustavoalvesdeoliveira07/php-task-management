<?php

include 'config.php';
include 'conexao.php';
include 'helpers.php';

$tem_erros = false;
$erros_validacao = array();

if (tem_post()) {
    // Upload dos anexos
    $tarefa_id = $_POST['tarefa_id'];

    if (!isset($_FILES['anexo'])) {
        $tem_erros = true;
        $erros_validacao['anexo'] = 'Você deve selecionar um arquivo para anexar';
    } else {
        if (validar_anexo($_FILES['anexo'])) {
            $anexo = array();
            $anexo['tarefa_id'] = $tarefa_id;
            $anexo['nome'] = $_FILES['anexo']['name'];
            $anexo['arquivo'] = $_FILES['anexo']['name'];
        } else {
            $tem_erros = true;
            $erros_validacao['anexo'] = 'Envie apenas anexos nos formatos .zip ou .pdf';
        }
    }

    if (!$tem_erros) {
        insert_anexo($conect, $anexo);
    }
}

$tarefa = get_tarefa($conect, $_GET['id']);
$anexos = get_anexos($conect, $_GET['id']);

include 'template_tarefa.php';

?>