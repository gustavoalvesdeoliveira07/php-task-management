<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="tarefa.css">
    <title>Gerenciador de Tarefas</title>
</head>
<body>
    <h1>Gerenciador de Tarefas</h1>

    <?php include 'formulario.php'; ?>

    <?php if($exibir_tarefas) : ?>
        <?php include 'tabela.php'; ?>
    <?php endif; ?>
</body>
</html>