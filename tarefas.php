<?php session_start() ?>
<!-- Session é usado para salvar os dados de uma requisição para outra
    // um requisição ocorre quando a página é atualizada (i.e.: ao clicar no botão de cadastrar) -->

<?php 

include 'config.php';
include 'conexao.php';
include 'helpers.php';

$exibir_tarefas = true;

$tem_erros = false;
$erros_validacao = array();

if ( tem_post() ) {
    $tarefa = array();

    if( isset($_POST['nome']) && strlen($_POST['nome']) > 0 ) {
        $tarefa['nome'] = $_POST['nome'];
    } else {
        $tem_erros = true;
        $erros_validacao['nome'] = 'O nome da tarefa é obrigatório!';
    }

    if ( isset($_POST['descricao']) )
        $tarefa['descricao'] = $_POST['descricao'];
    else
        $tarefa['descricao'] = '';

    if ( isset($_POST['prazo']) && strlen($_POST['prazo']) > 0 ) {
        if (validar_data($_POST['prazo'])) {
            $tarefa['prazo'] = date_form_to_database($_POST['prazo']);
        } else {
            $tem_erros = true;
            $erros_validacao['prazo'] = 'O prazo não é uma data válida!';
        }
    } else {
        $tarefa['prazo'] = '';
    }

    $tarefa['prioridade'] = $_POST['prioridade'];

    // input:checkbox -> quando não marcado, o navegador não envia o dado
    // por isso apenas a verificação no isset(...) é funcional
    if ( isset($_POST['concluida']) )
        $tarefa['concluida'] = 1;
    else
        $tarefa['concluida'] = 0;

    if(!$tem_erros) {
        insert_tarefa($conect, $tarefa);

        if (isset($_POST['lembrete']) && $_POST['lembrete'] == 1)
            enviar_email($tarefa);

        header('Location: tarefas.php');
        die();
    }
}

    /* Deixaremos de usar sessões
    if ( isset($_SESSION['lista_tarefas']) )
        $lista_tarefas = $_SESSION['lista_tarefas'];
    else
        $lista_tarefas = array();
    */
    // Usaremos um banco de dados
    $lista_tarefas = get_tarefas($conect);
?>

<?php 

$tarefa = array(
    'id' => 0,

    'nome' => (isset($_POST['nome'])) ? $_POST['nome'] : '',

    'descricao' => (isset($_POST['descricao'])) ? $_POST['descricao'] : '',

    'prazo' => (isset($_POST['prazo'])) ? date_form_to_database($_POST['prazo']) : '',

    'prioridade' => (isset($_POST['prioridade'])) ? $_POST['prioridade'] : 1,

    'concluida' => (isset($_POST['concluida'])) ? $_POST['concluida'] : ''
);

include 'template.php'; 
?>

