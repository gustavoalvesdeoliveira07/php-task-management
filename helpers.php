<?php

use PHPMailer\PHPMailer\PHPMailer;

function get_prioridade($prioridade) {
    if ($prioridade == 1) return 'Baixa';
    if ($prioridade == 2) return 'Média';
    if ($prioridade == 3) return 'Alta';
}

function get_situacao($situacao) {
    if ($situacao)
        return 'Sim';
    else
        return 'Não';
}

function date_form_to_database($data) {
    if ($data == '') return '';

    $dados = explode('/', $data);

    if (count($dados) != 3) return $data;

    $data_mysql = "{$dados[2]}-{$dados[1]}-{$dados[0]}";

    return $data_mysql;
}

function date_database_to_table($data_mysql) {
    if ($data_mysql == '' OR $data_mysql == '0000-00-00') return '';

    $dados = explode('-', $data_mysql);

    if (count($dados) != 3) return $data_mysql;

    $data = "{$dados[2]}/{$dados[1]}/{$dados[0]}";

    return $data;
}

function tem_post() {
    return (count($_POST) > 0) ? true : false;
}

function validar_data($data) {
    $padrao = '/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/';
    $resultado = preg_match($padrao, $data);

    if (!$resultado) return false;

    $dados = explode('/', $data);

    $dia = $dados[0];
    $mes = $dados[1];
    $ano = $dados[2];

    $resultado = checkdate($mes, $dia, $ano);

    return $resultado;
}

function validar_anexo($anexo) {
    $padrao = '/^.+(\.pdf|\.zip)$/';
    $resultado = preg_match($padrao, $anexo['name']);

    if (!$resultado)
        return false;

    move_uploaded_file($anexo['tmp_name'], "anexos/{$anexo['name']}");

    return true;
}

function enviar_email($tarefa, $anexos = array()) {
    // Incluindo a biblioteca
    // include 'bibliotecas/PHPMailer/PHPMailerAutoload.php';
    include 'bibliotecas/PHPMailer/...';

    $corpo = build_email_body($tarefa, $anexos);

    $email = new PHPMailer();

    $email->isSMTP();
    $email->Host = 'smtp.gmail.com';
    $email->Port = 587;
    $email->SMTPSecure = 'tls';
    $email->SMTPAuth = true;
    $email->Username = EMAIL_ORIGEM;
    $email->Password = EMAIL_PASSWORD;
    $email->setFrom('meuemail@email.com', 'Notificador de Tarefas');
    
    // Destinatário
    $email->addAddress(EMAIL_NOTIFICACAO);
    $email->Subject = "Aviso de tarefa: {$tarefa['nome']}";
    $email->msgHTML($corpo);

    foreach ($anexos as $anexo)
        $email->addAttachment("anexos/{$anexo['arquivo']}");

    $email->send();
}

function build_email_body($tarefa, $anexos) {
    // Falar para o PHP que não é para enviar o processamento
    // para o navegador
    ob_start();

    // Incluir o arquivo template_email.php
    include 'template_email.php';

    // Guardar o conteúdo do arquivo em uma variável
    $corpo = ob_get_contents();

    // Falar para o pHP que ele pode voltar a mandar conteúdos 
    // para o navegador
    ob_end_clean();

    return $corpo;
}

?>